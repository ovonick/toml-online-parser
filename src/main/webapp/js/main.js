angular.module('TomlOnlineParser', [])
    .controller('DefaultController', ['$scope', '$http', function($scope, $http) {
        $scope.displayProcessingStartedMessage = function() {
            $scope.parseResultsLabelCaption = "Processing...";
            $scope.parseResults = "";
        };

        $scope.displayProcessingFinishedMessage = function() {
            $scope.parseResultsLabelCaption = "Parsed Output";
        };

        $scope.initializeWithExampleFromTomlFile = function() {
            $http.get('example.toml').
                success(function(data, status, headers, config) {
                    $scope.toml = data;
                    $scope.onTomlChange();
                }).
                error(function(data, status, headers, config) {
                    $scope.toml = "[example.toml]\nfile=\"does not exist\"";
                    $scope.onTomlChange();
                });
        };

        $scope.onTomlChange = function() {
            $scope.displayProcessingStartedMessage();

            $http.
                post('/parsetoml', { toml: $scope.toml }, { transformResponse : function(data, headersGetter, status) {
                    // we do not want angularJS to parse json coming from server.
                    // we just want that json to be displayed
                    return data;
                }}).
                success(function(data, status, headers, config) {
                    $scope.displayProcessingFinishedMessage();
                    $scope.isError      = false;
                    $scope.parseResults = data;
                }).
                error(function(data, status, headers, config) {
                    $scope.displayProcessingFinishedMessage();
                    $scope.isError      = true;
                    var jsonObject      = JSON.parse(data);
                    var errorMessage    = jsonObject.error == "" ? "<empty>" : jsonObject.error;
                    var stackTrace      = jsonObject.stackTrace;
                    var isMalformedToml = jsonObject.isMalformedToml;

                    if (isMalformedToml) {
                        $scope.parseResults = errorMessage;
                    } else {
                        $scope.parseResults = "Error message: " + errorMessage + "\n\n" +
                                              "Stack Trace :\n" + stackTrace
                    }
                });
        };
        $scope.initializeWithExampleFromTomlFile();

    }]);
