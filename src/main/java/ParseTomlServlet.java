import tomlette.Parser;
import tomlette.TomletteException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * @author ovonick
 */
public class ParseTomlServlet extends HttpServlet {

    private static final String JSON_CONTENT_TYPE = "application/json; charset=UTF-8";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType(JSON_CONTENT_TYPE);

        String responseString;
        try (StringReader stringReader = new StringReader(getTomlString(request))) {
            responseString = Parser.parse(stringReader).toString();
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception exception) {
            responseString = getExceptionAsResponseJsonString(exception);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

        response.getWriter().print(responseString);
        response.getWriter().flush();
    }

    private String getExceptionAsResponseJsonString(Exception exception) throws IOException {
        String message = exception.getMessage() == null ? "" : exception.getMessage();

        JsonObject jsonObject = Json.createObjectBuilder()
                .add("error", message)
                .add("isMalformedToml", exception instanceof TomletteException)
                .add("stackTrace", getStackTraceAsString(exception))
                .build();
        return jsonObject.toString();
    }

    private String getStackTraceAsString(Exception exception) throws IOException {
        try (StringWriter stringWriter = new StringWriter();
             PrintWriter  printWriter  = new PrintWriter(stringWriter)) {
            exception.printStackTrace(printWriter);
            return stringWriter.toString();
        }
    }

    private String getTomlString(HttpServletRequest request) {
        try (JsonReader jsonReader = Json.createReader(request.getReader())) {
            return jsonReader.readObject().getString("toml");
        } catch (IOException e) {
            throw new RuntimeException("Could not extract json object from request payload");
        }
    }
}
